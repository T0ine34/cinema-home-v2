import numpy as np
import cv2
from mss import mss
from PIL import Image
import time

from threading import Thread
from queue import Queue

from process import process_image


from sys import argv


from json import loads


SCREENS = loads(open("screen.jsonc").read())


args = argv[1:]

console = True
debug = False
sides = True
if len(args) > 0:
    for arg in args:
        if arg.startswith("--"):
            arg = arg[2:]
            if arg == "no-console":
                console = False
            elif arg == "debug":
                debug = True
            elif arg == "no-sides":
                sides = False
            elif arg == "help":
                print("usage: python3 screen.py [-c] [-d] [-s] [--no-console] [--debug] [--no-sides]")
                exit(0)
            else:
                print("unknown arg: --" + arg)
                exit(1)
        elif arg.startswith("-"):
            arg = arg[1:]
            for letter in arg:
                if letter == "c":
                    console = False
                elif letter == "d":
                    debug = True
                elif letter == "s":
                    sides = False
                elif letter == "h":
                    print("usage: python3 screen.py [-c] [-d] [-s] [--no-console] [--debug] [--no-sides]")
                    exit(0)
                else:
                    print("unknown arg: -" + letter)
                    exit(1)
        else:
            print("unknown arg: " + arg)
            print("usage: python3 screen.py [-c] [-d] [-s] [--no-console] [--debug] [--no-sides]")
            exit(1)



width_relative = 16 #the width of the screen is divided by this number to get the width of each side

main_screen = SCREENS["items"][SCREENS["main"]]

left_screen = SCREENS["items"]["left"] if "left" in SCREENS["items"].keys() else None
right_screen = SCREENS["items"]["right"] if "right" in SCREENS["items"].keys() else None

bounding_box_left = {
    "top": main_screen["y"],
    "left": main_screen["x"],
    "width": main_screen["width"]//width_relative,
    "height": main_screen["height"]
}
bounding_box_right = {
    "top": main_screen["y"],
    "left": main_screen["x"]+(width_relative-1)*main_screen["width"]//width_relative,
    "width": main_screen["width"]//width_relative,
    "height": main_screen["height"]
}

sct = mss() #create a new mss instance, which is used to take screenshots

if left_screen:
    IMG_QUEUE_LEFT = Queue()
if right_screen:
    IMG_QUEUE_RIGHT = Queue()

THREADS = []

start_time = time.time()
frame_count = 0

if console:
    print(bounding_box_left)
    print(bounding_box_right)

RUNNING = True




def showboxes():
    global RUNNING
    #draw the bounding boxes as two red rectangles on a separate window, with a transparent background
    
    import tkinter as tk
    
    root = tk.Tk()
    root.attributes("-transparentcolor", "white")
    root.overrideredirect(True)
    root.geometry("+0+0")
    root.lift()
    root.wm_attributes("-topmost", True)
    root.wm_attributes("-transparentcolor", "white")
    root.configure(background='white')

    canvas = tk.Canvas(root, width = main_screen["width"], height = main_screen["height"], bg="white", highlightthickness=0)
    canvas.pack()

    canvas.create_rectangle(0-1, 0-1, main_screen["width"]//width_relative, main_screen["height"], outline="red")
    canvas.create_rectangle(((width_relative-1)*main_screen["width"]//width_relative)-1, 0-1, main_screen["width"], main_screen["height"], outline="red")

    def close(event):
        global RUNNING
        RUNNING = False
        root.destroy()
        
    root.bind("<Escape>", close)
    root.bind("<q>", close)

    while RUNNING:
        root.update()
        time.sleep(0.01)



if debug:
    if console:
        print("debug mode")
    th = Thread(target=showboxes)
    th.start()
    THREADS.append(th)

if console:
    print("press q to quit")





#display images in the queue in parallel
def show_images():
    global RUNNING
    while RUNNING:
        if right_screen:
            right = IMG_QUEUE_RIGHT.get()
            cv2.namedWindow('right', cv2.WINDOW_NORMAL)
            cv2.moveWindow('right', right_screen["x"], right_screen["y"])
            cv2.setWindowProperty("right", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow('right', right)
        
        if left_screen:
            left = IMG_QUEUE_LEFT.get()
            cv2.namedWindow('left', cv2.WINDOW_NORMAL)
            cv2.moveWindow('left', left_screen["x"], left_screen["y"])
            cv2.setWindowProperty("left", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow('left', left)
            
            
            
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            RUNNING = False
            cv2.destroyAllWindows()
        
if sides:
    show_thread = Thread(target=show_images)
    show_thread.start()
    THREADS.append(show_thread)


def process(image : np.ndarray, res_queue  : Queue, side : str):
    width = SCREENS["items"][side]["width"]
    height = SCREENS["items"][side]["height"]
    image = process_image(image, width, height, side)
    res_queue.put(image)


while RUNNING:
    
    if left_screen:
        sct_img_left = np.array(sct.grab(bounding_box_left))
        sct_img_left = cv2.resize(sct_img_left, (SCREENS["items"]["left"]["width"], SCREENS["items"]["left"]["height"]))
        th = Thread(target=process, args=(sct_img_left, IMG_QUEUE_LEFT,"left"))
        th.start()
        THREADS.append(th)
    
    if right_screen:
        sct_img_right = np.array(sct.grab(bounding_box_right))
        sct_img_right = cv2.resize(sct_img_right, (SCREENS["items"]["right"]["width"], SCREENS["items"]["right"]["height"]))
        th = Thread(target=process, args=(sct_img_right, IMG_QUEUE_RIGHT,"right"))
        th.start()
        THREADS.append(th)
    

    if (cv2.waitKey(1) & 0xFF) == ord('q'):
        RUNNING = False
        cv2.destroyAllWindows()

    frame_count += 1
    if frame_count % 10 == 0:
        elapsed_time = time.time() - start_time
        if console:
            fps = frame_count / elapsed_time
            print(f"\rFPS: {fps:.2f}",end="")



cv2.destroyAllWindows()
RUNNING = False
if console:
    print()

for th in THREADS:
    th.join()