import numpy as np
import cv2

def process_image(image : np.array, width : int, height : int, side : str) -> np.array:
    #mirror the image, blur it, then exetend it to the full width of the screen
    ksize = (480,540)
    image = cv2.flip(image, 1)
    
    #add a black bar to the side of the image of 1/8 the width
    if side == "right":
        image = np.concatenate((image, np.zeros((height, width//8, 4), np.uint8)), axis=1)
    elif side == "left":
        image = np.concatenate((np.zeros((height, width//8, 4), np.uint8), image), axis=1)
        
    image = cv2.blur(image, ksize)

    return image