import cv2

from json import loads

from sys import argv

import numpy as np

if len(argv) < 2:
    print("Usage: python3 white.py <screen(s) name(s)>")
    exit(1)

screens_names = argv[1:]

for screen_name in screens_names:
    with open("screen.jsonc", "r") as f:
        config = loads(f.read())
        x = config["items"][screen_name]["x"]
        y = config["items"][screen_name]["y"]
        
        width = config["items"][screen_name]["width"]
        height = config["items"][screen_name]["height"]
        
    cv2.namedWindow(screen_name, cv2.WINDOW_NORMAL)
    cv2.moveWindow(screen_name, x, y)
    cv2.setWindowProperty(screen_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    
    img = np.zeros((height, width, 3), np.uint8)
    img.fill(255)

    cv2.imshow(screen_name, img)
    
cv2.waitKey(0) #wait for a keypress

