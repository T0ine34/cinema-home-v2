import numpy as np
import cv2
from mss import mss
from PIL import Image
import time

from threading import Thread
from queue import Queue

from process import process_image


from sys import argv


from json import loads


console = True
debug = False
sides = True


SCREENS = loads(open("screen.jsonc").read())

main_screen = SCREENS["items"][SCREENS["main"]]

left_screen = SCREENS["items"]["left"] if "left" in SCREENS["items"].keys() else None
right_screen = SCREENS["items"]["right"] if "right" in SCREENS["items"].keys() else None


if left_screen:
    IMG_QUEUE_LEFT = Queue()
if right_screen:
    IMG_QUEUE_RIGHT = Queue()

THREADS = []

RUNNING = True

#display images in the queue in parallel
def show_images():
    global RUNNING
    while RUNNING:
        if right_screen:
            right = IMG_QUEUE_RIGHT.get()
            cv2.namedWindow('right', cv2.WINDOW_NORMAL)
            cv2.moveWindow('right', right_screen["x"], right_screen["y"])
            cv2.setWindowProperty("right", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow('right', right)
        
        if left_screen:
            left = IMG_QUEUE_LEFT.get()
            cv2.namedWindow('left', cv2.WINDOW_NORMAL)
            cv2.moveWindow('left', left_screen["x"], left_screen["y"])
            cv2.setWindowProperty("left", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow('left', left)
            
            
            
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            RUNNING = False
            cv2.destroyAllWindows()
        
if sides:
    show_thread = Thread(target=show_images)
    show_thread.start()
    THREADS.append(show_thread)
    
if console:
    print("press q to quit")
    
    

def process(image : np.ndarray, res_queue  : Queue, side : str):
    width = SCREENS["items"][side]["width"]
    height = SCREENS["items"][side]["height"]
    image = process_image(image, width, height, side)
    res_queue.put(image)